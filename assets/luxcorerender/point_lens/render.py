#! /usr/bin/env python

# mesh-00000 = lens
# mesh-00001 = cache
# mesh-00002 = screen

luxcorerender_path = "/opt/luxrender/bin"

import sys
sys.path.append(luxcorerender_path)
import os
os.environ["LD_LIBRARY_PATH"] = luxcorerender_path

import time
import pyluxcore

verbose = True

pyluxcore.Init()

# Load the configuration from file
props = pyluxcore.Properties("render.cfg")
config = pyluxcore.RenderConfig(props)
session = pyluxcore.RenderSession(config)

session.Start()

start_time = time.time()
halt_time = 30
update_stats_time = 10.0
elapsed_time = 0.0

while elapsed_time < halt_time:
    time.sleep(update_stats_time)
    elapsed_time = time.time() - start_time

    # Update statistics
    session.UpdateStats()

    stats = session.GetStats()
    render_time     = stats.Get("stats.renderengine.time").GetFloat()
    samples_per_sec = stats.Get("stats.renderengine.total.samplesec").GetFloat()  / 1000000 # Msample/s
    npass           = stats.Get("stats.renderengine.pass").GetInt()
    triangles       = stats.Get("stats.dataset.trianglecount").GetFloat() / 1000 # ktriangles
    convergence     = stats.Get("stats.renderengine.convergence").GetFloat()

    if verbose:
        print('[Elapsed time: %3d/%3dsec][Samples %4d][Avg. samples/sec % 3.2fM on %.1fK tris] convergence=%0.8f' % (
            render_time, halt_time, samples_per_sec, npass, triangles, convergence))

session.Stop()
session.GetFilm().Save()
