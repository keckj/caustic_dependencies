#!/usr/bin/env bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
set -e

mkdir -p BlendLuxCore
cd BlendLuxCore

mkdir -p bin
cd bin
ln -sf ${SCRIPT_DIR}/../LuxCore/build/lib/pyluxcoretools.zip 
ln -sf ${SCRIPT_DIR}/../LuxCore/build/lib/pyluxcore.so 
ln -sf ${SCRIPT_DIR}/../BlendLuxCore/bin/__init__.py 
cd -

for d in $(ls "${SCRIPT_DIR}/../BlendLuxCore"); do
    if [[ "$d" != "bin" ]]; then 
        d="${SCRIPT_DIR}/../BlendLuxCore/$d"
        if [[ -d "$d" ]]; then
            ln -sf $d 
        fi
    fi
done
for f in $(ls -d1 ${SCRIPT_DIR}/../BlendLuxCore/*); do
    if [[ -f "$f" ]]; then
        ln -sf $f 
    fi
done

cd $SCRIPT_DIR

zip -r BlendLuxCore-v2.2-blender2.80-linux64-opencl.zip -x'BlendLuxCore/.gitignore' -x'BlendLuxCore/azure-pipelines.yml' -x'BlendLuxCore/scripts/*' BlendLuxCore
