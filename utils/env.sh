
export CAUSTICS_ROOT="$HOME/Documents/caustic-design"
export POSTDOC_ROOT="$HOME/Documents/postdoc"
export BLENDER_ROOT="/opt/blender"
export LUXCORE_ROOT="/opt/luxcore"

alias cdc="cd $CAUSTICS_ROOT"
alias cdp="cd $POSTDOC_ROOT"

COMPILERVARS_ARCHITECTURE=intel64 COMPILEVARS_PLARFORM=linux source /opt/intel/bin/compilervars.sh

export PYTHONPATH="$CAUSTICS_ROOT/src:$PYTHONPATH"
export PATH="${BLENDER_ROOT}/bin:${LUXCORE_ROOT}/bin:${PATH}"

# Clean up duplicates
export PATH=$(echo $PATH | sed 's/;//g' | xargs -n1 -d':' | awk '!seen[$0]++' | paste -sd':')
export LD_LIBRARY_PATH=$(echo $LD_LIBRARY_PATH | sed 's/;//g' | xargs -n1 -d':' | awk '!seen[$0]++' | paste -sd':')
