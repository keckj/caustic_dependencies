#!/usr/bin/env bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
${SCRIPT_DIR}/deviceQuery | grep 'CUDA Capability' | grep -o -e '[0-9]\+\.[0-9]\+' | sed 's/\.//g' | sort -nu | paste -sd';'
