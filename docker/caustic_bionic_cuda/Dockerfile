# Test docker for gitlab-ci
FROM nvidia/cuda:10.2-devel-ubuntu18.04
MAINTAINER Jean-Baptiste.Keck@imag.fr

ARG CUDA_COMPUTE_CAPABILITY
ARG NTHREADS

ARG DEPS_REPO=https://gitlab.com/keckj/caustic_dependencies.git
ARG DEPS_ROOT=/tmp/dependencies
ARG DEPS_TAG=2fc4df6

ENV DEBIAN_FRONTEND=noninteractive

# upgrade initial image
RUN apt-get update
RUN apt-get install -y apt-utils git
RUN apt-get dist-upgrade -y

# versioned dependencies
RUN git clone ${DEPS_REPO} ${DEPS_ROOT} && \
cd ${DEPS_ROOT} && \
git checkout ${DEPS_TAG} && \
git submodule update --init --recursive --jobs=${NTHREADS}

# system packages
RUN apt-get install -y vim ssh bash-completion rsync nasm cpio unzip zip p7zip-full patchelf
RUN apt-get install -y build-essential automake libtool pkg-config make cmake bison flex g++-multilib
RUN apt-get install -y libicu-dev libzstd-dev libssl-dev libelf-dev libffi-dev libncurses5-dev libblosc-dev 
RUN apt-get install -y libtiff5-dev libopencolorio-dev libopenjp2.7-dev libwebp-dev libjpeg8-dev libpng-dev libsquish-dev
RUN apt-get install -y libdcmtk-dev libgif-dev libraw-dev libfftw3-dev opencollada-dev libspnav-dev spacenavd libopenal-dev 
RUN apt-get install -y libx264-dev libx265-dev libde265-dev libjemalloc-dev libopencv-dev libopencv-calib3d-dev
RUN apt-get install -y libopenmpi-dev libhdf5-dev libhdf5-mpi-dev libcgal-dev libeigen3-dev pyside-tools
RUN apt-get install -y libpython2.7-dev python2.7-dev libpython3.7-dev python3.7-dev
RUN apt-get install -y xz-utils libbz2-dev zlib1g-dev libz3-dev liblzma-dev
RUN apt-get install -y libgtk-3-dev qtwayland5 libfltk1.3-dev freeglut3-dev
RUN apt-get install -y opencl-headers ocl-icd-libopencl1 clinfo

# PYTHON 2.7 + PIP 2.7 + required python modules 
RUN wget https://bootstrap.pypa.io/get-pip.py && python2.7 get-pip.py && rm -f get-pip.py
RUN pip2.7 install --upgrade pip
RUN pip2.7 install --upgrade numpy
RUN pip2.7 install --upgrade request
RUN pip2.7 install --upgrade requests

# PYTHON 3.7 + PIP 3.7 + required python modules
RUN wget https://bootstrap.pypa.io/get-pip.py && python3.7 get-pip.py && rm -f get-pip.py
RUN pip3.7 install --upgrade pip
RUN pip3.7 install --upgrade numpy
RUN pip3.7 install --upgrade request
RUN pip3.7 install --upgrade requests

# BOOST 1.70.0 with python 2.7 and python 3.7 support
RUN cd ${DEPS_ROOT}/deps/boost && \
echo 'using mpi ; ' > ./tools/build/src/user-config.jam && \
echo 'using python : 2.7 : /usr/bin/python2.7 : /usr/include/python2.7 : /usr/lib/python2.7 ;' >> ./tools/build/src/user-config.jam && \
echo 'using python : 3.7 : /usr/bin/python3.7 : /usr/include/python3.7 : /usr/lib/python3.7 ;' >> ./tools/build/src/user-config.jam && \
./bootstrap.sh --prefix=/opt/boost/1.70.0 && \
./b2 stage python=2.7,3.7 threading=multi link=shared -j${NTHREADS} && \
./b2 install python=2.7 threading=multi link=shared && \
./b2 install python=3.7 threading=multi link=shared && \
./b2 install

# CMAKE
RUN cd ${DEPS_ROOT}/deps/CMake && \
mkdir build && cd build && \
cmake -DCMAKE_BUILD_TYPE=Release .. && \
make -j$NTHREADS && \
make install

# LLVM-PROJECT (llvm + clang + clang-tools + openmp + parallel libs)
RUN cd ${DEPS_ROOT}/deps/llvm-project && \
sed -i 's/llvm::Twine(Z3_get_error_msg(Context, Error))/std::string("")/g' clang/lib/StaticAnalyzer/Core/Z3ConstraintManager.cpp && \
mkdir build && cd build && \
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_FLAGS=-DLLVM_ENABLE_DUMP -DCMAKE_CXX_FLAGS=-DLLVM_ENABLE_DUMP -DLLVM_ENABLE_ASSERTIONS=ON -DLLVM_ENABLE_PROJECTS="compiler-rt;clang;clang-tools-extra;openmp;parallel-libs" -DLLVM_TARGETS_TO_BUILD="X86;NVPTX" -DCLANG_OPENMP_NVPTX_DEFAULT_ARCH=sm_${CUDA_COMPUTE_CAPABILITY}  -DLIBOMPTARGET_NVPTX_COMPUTE_CAPABILITIES=${CUDA_COMPUTE_CAPABILITY} -DLLVM_BUILD_LLVM_DYLIB=ON -DCMAKE_INSTALL_PREFIX=/opt/llvm/llvm-8.0.1 ../llvm && \
make -j${NTHREADS} && \
make install

# QT5
RUN cd ${DEPS_ROOT}/deps/qli-installer && \
python3.7 ./qli-installer.py 5.14.0 linux desktop && \
mkdir -p /opt/qt && \
mv 5.14.0 /opt/qt/

# Intel TBB
RUN cd ${DEPS_ROOT}/deps/tbb && \
make -j${NTHREADS} && \
mkdir -p /opt/intel/tbb && cp -r ./include /opt/intel/tbb/include && \
mkdir -p /opt/intel/tbb/lib && find -name '*.so*' -exec mv -t /opt/intel/tbb/lib/ {} +

# Intel OpenCL
RUN cd /tmp && \
wget http://registrationcenter-download.intel.com/akdlm/irc_nas/vcp/15532/l_opencl_p_18.1.0.015.tgz && \
tar -xvzf l_opencl_p_18.1.0.015.tgz && \
cd l_opencl_p_18.1.0.015 && \
sed -i "s/ACCEPT_EULA=decline/ACCEPT_EULA=accept/g" "silent.cfg" && \
./install.sh --silent ./silent.cfg

# Fix OpenCL NVidia ICD
RUN echo "/usr/lib/x86_64-linux-gnu/libnvidia-opencl.so.1" > /etc/OpenCL/vendors/nvidia.icd

# NVidia CG
RUN cd /tmp && \
wget http://developer.download.nvidia.com/cg/Cg_3.1/Cg-3.1_April2012_x86_64.tgz && \
tar -xvzf Cg-3.1_April2012_x86_64.tgz && \
rsync -r ./usr/ /usr

# JPEG-TURBO
RUN cd ${DEPS_ROOT}/deps/libjpeg-turbo && \
mkdir build && cd build && \
cmake -DCMAKE_BUILD_TYPE=Release .. && \
make -j${NTHREADS} && \
make install

# PTEX
RUN cd ${DEPS_ROOT}/deps/ptex && \
make prefix=/usr/local -j${NTHREADS} && \
make install

# OPENEXR
RUN cd ${DEPS_ROOT}/deps/openexr && \
mkdir build && cd build && \
cmake -DCMAKE_BUILD_TYPE=Release -DBoost_NO_SYSTEM_PATHS=ON -DBOOST_ROOT=/opt/boost/1.70.0 .. && \
make -j${NTHREADS} && \
make install

# LIBHEIF + headers for OIIO
RUN cd ${DEPS_ROOT}/deps/libheif && \
mkdir build && cd build && \
CFLAGS=-I$(pwd) cmake .. && \
CFLAGS=-I$(pwd) make -j${NTHREADS} && \
make install && \
cp ../libheif/*.h libheif/

# ALEMBIC
RUN cd ${DEPS_ROOT}/deps/alembic && \
mkdir build && cd build && \
cmake -DCMAKE_BUILD_TYPE=Release -DUSE_HDF5=ON -DUSE_MAYA=OFF -DUSE_ARNOLD=OFF -DUSE_PYALEMBIC=OFF -DUSE_PRMAN=OFF -DUSE_TESTS=OFF -DUSE_EXAMPLES=OFF .. && \
make -j${NTHREADS} && \
make install

# OPENVDB
RUN cd ${DEPS_ROOT}/deps/openvdb && \
mkdir build && cd build && \
cmake -DCMAKE_BUILD_TYPE=Release -DBoost_NO_SYSTEM_PATHS=ON -DBOOST_ROOT=/opt/boost/1.70.0 -DTBB_ROOT=/opt/intel/tbb .. && \
make -j${NTHREADS} && \
make install

# OIIO
RUN cd ${DEPS_ROOT}/deps/oiio && \
mkdir build && cd build && \
CLANG_FORMAT_EXE_HINT=/opt/llvm/llvm-8.0.1/bin cmake -DCMAKE_BUILD_TYPE=Release -DJPEGTurbo_ROOT=/opt/libjpeg-turbo -DLIBHEIF_INCLUDE_DIR="${DEPS_ROOT}/deps/libheif/build" -DBoost_NO_SYSTEM_PATHS=ON -DBOOST_ROOT=/opt/boost/1.70.0 -DQt5_ROOT=/opt/qt/5.14.0/gcc_64 -DTBB_ROOT=/opt/intel/tbb -DPYTHON_EXECUTABLE=/usr/bin/python3.7 .. && \
make -j${NTHREADS} && \
find . -name '*.so*' -type f -executable -exec patchelf --replace-needed libtbb.so.2 /opt/intel/tbb/lib/libtbb.so.2 {} \; && \
make install

# ISPC (nvptx target has been deprecated and does not compile anymore so we disable it)
RUN cd ${DEPS_ROOT}/deps/ispc && \
sed -i 's/set(CMAKE_C_COMPILER "clang")/set(CMAKE_C_COMPILER "\/opt\/llvm\/llvm-8.0.1\/bin\/clang")/g' CMakeLists.txt && \
sed -i 's/set(CMAKE_CXX_COMPILER "clang++")/set(CMAKE_CXX_COMPILER "\/opt\/llvm\/llvm-8.0.1\/bin\/clang++")/g' CMakeLists.txt && \
mkdir build && cd build && \
cmake -DLLVM_DIR=/opt/llvm/llvm-8.0.1/lib/cmake/llvm -DCMAKE_BUILD_TYPE=Release -DARM_ENABLED=OFF -DNVPTX_ENABLED=OFF -DISPC_INCLUDE_TESTS=OFF .. && \
make -j${NTHREADS} && \
make install

# EMBREE (setting rpath does not always work for some reason so we use the patchelf utility)
RUN cd ${DEPS_ROOT}/deps/embree && \
mkdir build && cd build && \
cmake -DCMAKE_BUILD_TYPE=Release -DEMBREE_TUTORIALS=OFF -DEMBREE_TBB_ROOT=/opt/intel/tbb .. && \
make -j${NTHREADS} && \
find . -name '*.so*' -type f -executable -exec patchelf --replace-needed libtbb.so.2 /opt/intel/tbb/lib/libtbb.so.2 {} \; && \
make install

# OIDN
RUN cd ${DEPS_ROOT}/deps/oidn && \
mkdir build && cd build && \
cmake -DCMAKE_BUILD_TYPE=Release -DTBB_ROOT=/opt/intel/tbb -DPYTHON_EXECUTABLE=/usr/bin/python3.7 .. && \
make -j${NTHREADS} && \
find . -type f -executable -exec patchelf --replace-needed libtbb.so.2 /opt/intel/tbb/lib/libtbb.so.2 {} \; && \
make install

# LuxCore
RUN cd ${DEPS_ROOT}/deps/LuxCore && \
mkdir build && cd build && \
cmake -DCMAKE_BUILD_TYPE=Release -DOpenGL_GL_PREFERENCE=LEGACY -DBoost_NO_SYSTEM_PATHS=ON -DBOOSTROOT=/opt/boost/1.70.0 -DPYTHON_V=37 -DCMAKE_CXX_FLAGS='-L/opt/intel/tbb/lib' .. && \
make -j${NTHREADS} && \
find bin/ -type f -executable -exec patchelf --set-rpath /opt/boost/1.70.0/lib {} \; && \
find bin/ -type f -executable -exec patchelf --replace-needed libembree3.so.3 /usr/local/lib/libembree3.so.3 {} \; && \
find lib/ -type f -executable -exec patchelf --set-rpath /opt/boost/1.70.0/lib {} \; && \
find lib/ -type f -executable -exec patchelf --replace-needed libembree3.so.3 /usr/local/lib/libembree3.so.3 {} \; && \
mkdir -p /opt/luxcore && \
cp -r bin /opt/luxcore/ && \
cp -r lib /opt/luxcore/ && \
cp -r samples /opt/luxcore/ && \
rm -f /opt/luxcore/lib/*.a

# LUXRENDER (blendluxcore python module)
RUN cd ${DEPS_ROOT}/deps/luxrender && \
make

# BLENDER
RUN cd ${DEPS_ROOT}/deps/blender && \
mkdir build && cd build && \
ln -s /usr/lib/x86_64-linux-gnu/libpython3.7m.so /usr/lib/x86_64-linux-gnu/libpython3.7.so && \
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/blender -DPYTHON_VERSION=3.7 -DPYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython3.7.so -DPYTHON_INCLUDE_DIR=/usr/include/python3.7 -DPYTHON_LIBPATH=/usr/local/lib -DTBB_ROOT_DIR=/opt/intel/tbb -DBoost_NO_SYSTEM_PATHS=ON -DBOOST_ROOT=/opt/boost/1.70.0 -DWITH_FFTW3=ON -DWITH_OPENCOLLADA=ON -DWITH_OPENCOLORIO=ON -DWITH_OPENIMAGEDENOISE=ON -DWITH_OPENVDB=ON -DWITH_ALEMBIC=ON -DWITH_PYTHON_INSTALL=ON -DWITH_INSTALL_PORTABLE=OFF .. && \
make -j${NTHREADS} && \
make install && \
find /opt/blender/bin/ -type f -executable -exec patchelf --set-rpath /opt/boost/1.70.0/lib {} \;

# ensure all libraries are known by the runtime linker
#RUN ldconfig

# clean cached packages
#RUN rm -rf /var/lib/apt/lists/*
#RUN rm -rf $HOME/.cache/pip/*
#RUN rm -rf /tmp/*

CMD ["/bin/bash"]
